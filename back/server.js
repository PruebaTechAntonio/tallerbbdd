var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

//variable para poder usar el request-json
var requestjson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // for parsing application/json

var path = require('path');

//para solucionar el tema de CORS
var cors = require ('cors');
app.use(cors());

////////////////////////////////
// para conectarse con el MongoDB en local

var mongoClient = require('mongodb').MongoClient;
//var url = "mongodb://localhost:27017/local"; // si ponemos el mongo en un docker
// en lugar de localhost tendriamos que poner servermongo o el nomrbre que pongamos
// en el docker
var url = "mongodb://servermongo:27017/local";


///////////////////////////////
// para conectarme con PostgreSQL

var pg = require('pg');
//var urlUsuarios = "postgres://docker:docker@serverpostgre:5432/bdseguridad"; // en local sería localhost:5433
var urlUsuarios = "postgres://docker:docker@localhost:5433/bdseguridad"; // en local sería localhost:5433

var clientePostgre = new pg.Client(urlUsuarios);


//////////////////////////////////






var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos";
var apiKey = "apiKey=50c5ea68e4b0a97d668bc84a";
var clienteMlab = requestjson.createClient(urlmovimientosMlab + "?" + apiKey);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/movimientos', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
  clienteMlab = requestjson.createClient(urlmovimientosMlab + "?" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});


//hacemos el get de movimientos llamando al Mongo local
app.get('/movimientosLocal', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
  mongoClient.connect(url, function(err,db) {
    if (err)
    {
      console.log(err);
    }
    else
    {
        var col = db.collection('movimientos');
        console.log("Connected succesfully to server");

        //col.find({}).limit(3).toArray(function(err,docs) { // limitando a 3 resultados
        col.find({}).toArray(function(err,docs) {
        res.send(docs);
        });
        db.close();
    }
  });
});

//hacemos el post de movimientos llamando al Mongo local
app.post('/movimientosLocal', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
  mongoClient.connect(url, function(err,db) {
    if (err)
    {
      console.log(err);
    }
    else
    {
        var col = db.collection('movimientos');
        console.log("Trying to insert to server");
        // db.collection('movimientos').insertOne({a:1}, function(err,r) {
        //   console.log(r.insertCount + 'registros insertados');
        // });
        // //Insert multiple documents
        // db.collection('movimientos').insertMany([{a:2}, {a:3}], function(err,r) {
        //   console.log(r.insertCount + 'registros insertados');
        // });
        //Insert one document from body
        // db.collection('movimientos').insertOne(req.body, function(err,r) {
        //   console.log(r.insertCount + 'registros insertados');
        // });

        // Insertamos el body con los clientes
        db.collection('movimientos').insert(req.body, function(err,r) {
           console.log(r.insertCount + 'registros insertados');
         });

        db.close();
        res.send("ok");
    }
  });
});


app.get('/clientes', function(req, res) {
  //res.sendFile(path.join(__dirname, 'index.html'));
  clienteMlab = requestjson.createClient(urlmovimientosMlab + "?f={'idcliente':1, 'nombre': 1, 'apellidos': 1}" + "&" + apiKey);
    clienteMlab.get('', function(err, resM, body) {
      if(err)
      {
        console.log(body);
      }
      else
      {
        res.send(body);
      }
    });
});

app.post('/login', function(req, res) {
  //Crear cliente PostgreSQL --> lo tenemos arriba como global
  //var clientePostgre = new pg.Client(urlUsuarios);
  clientePostgre.connect();
    // asumimos que recibimos req.body = {usuario:xxxx, password:yyyy}
  // Hacer consulta
  const query = 
    clientePostgre.query('SELECT COUNT(*) FROM usuarios WHERE usuario=$1 AND password=$2;',
    [req.body.usuario, req.body.password],
    (err, result) => {
      if (err) {
        console.log(err);
        res.send(err);
      }
      else{
        if (result.rows[0].count >= 1){
          res.send("Login correcto ");
        }
        else{
          res.send("Login incorrecto");
        }
        //console.log(result.rows[0]); //Estamos asumiendo que siempre hay uno, habria que 
                                    // validar que no es vacio y tiene al menos uno
        //res.send(result.rows[0]);
      }
    }
  );
  //Devolver resultado
});



app.post('/movimientos', function(req, res) {
  /* Crear movimiento en MLab */

});

app.get('/movimientos/:idcliente', function (req, res) {
  /* Obtener movimientos del cliente idcliente desde MLab */
});
